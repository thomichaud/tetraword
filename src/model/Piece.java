package model;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.util.Random;

/*
 *  Class Piece :
 * 		- numero
 * 		- currentPiece
 * 		- currentPosition
 * 
 * 	Methodes :
 * 		- Initialisation
 * 		- creerPiece()
 * 		- currentPosition(angle)
 * 		- rotation(angle) <-- appelle currentPosition(angle) pour changer la position actuelle de la piece en jeu
 * 		- getType() <- renvoi le numero du type de la piece
 */

public class Piece extends Component {

	public int numero;
	public int [][] currentPiece;
	public int [][] currentPosition;
	public int countRotation=0;
	
	int [][] piece1 = {
		{0,0,0,0,0,0,0,0,0,1,1,0,0,1,1,0},
		{0,0,0,0,0,0,0,0,0,1,1,0,0,1,1,0},
		{0,0,0,0,0,0,0,0,0,1,1,0,0,1,1,0},
		{0,0,0,0,0,0,0,0,0,1,1,0,0,1,1,0}
	};
	
	int [][] piece2 = {
		{0,2,0,0,0,2,0,0,0,2,0,0,0,2,0,0},
		{0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0},
		{0,0,2,0,0,0,2,0,0,0,2,0,0,0,2,0},
		{0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0}
	};

	int [][] piece3 = {
			{0,0,0,0,0,3,0,0,0,3,3,0,0,0,3,0},
			{0,0,0,0,0,0,0,0,0,0,3,3,0,3,3,0},
			{0,0,0,0,0,3,0,0,0,3,3,0,0,0,3,0},
			{0,0,0,0,0,0,0,0,0,0,3,3,0,3,3,0}
		};
	
	int [][] piece4 = {
			{0,0,0,0,0,0,4,0,0,4,4,0,0,4,0,0},
			{0,0,0,0,0,4,4,0,0,0,4,4,0,0,0,0},
			{0,0,0,0,0,0,4,0,0,4,4,0,0,4,0,0},
			{0,0,0,0,0,4,4,0,0,0,4,4,0,0,0,0}
		};
	
	int [][] piece5 = {
			{0,0,0,0,0,5,0,0,0,5,5,0,0,5,0,0},
			{0,0,0,0,0,0,0,0,0,0,5,0,0,5,5,5},
			{0,0,0,0,0,0,5,0,0,5,5,0,0,0,5,0},
			{0,0,0,0,0,0,0,0,0,5,5,5,0,0,5,0}
		};
	
	int [][] piece6 = {
			{0,0,0,0,0,6,0,0,0,6,0,0,0,6,6,0},
			{0,0,0,0,0,0,0,0,0,0,0,6,0,6,6,6},
			{0,0,0,0,0,6,6,0,0,0,6,0,0,0,6,0},
			{0,0,0,0,0,0,0,0,0,6,6,6,0,6,0,0}
		};
	
	int [][] piece7 = {
			{0,0,0,0,0,0,7,0,0,0,7,0,0,7,7,0},
			{0,0,0,0,0,0,0,0,0,7,7,7,0,0,0,7},
			{0,0,0,0,0,7,7,0,0,7,0,0,0,7,0,0},
			{0,0,0,0,0,0,0,0,0,7,0,0,0,7,7,7}
		};
	
	public void creerPiece(int numero){
		
		this.currentPiece = new int [4][16];
		countRotation=0;
		
		switch (numero) {
        	case 1:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece1[i][j];
        				this.numero = 1;
        			}
        		};
        	break;
        	case 2:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece2[i][j];
        				this.numero = 2;
        			}
        		};
        	break;
        	case 3:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece3[i][j];
        				this.numero = 3;
        			}
        		};
        	break;
        	case 4:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece4[i][j];
        				this.numero = 4;
        			}
        		};
        	break;
        	case 5:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece5[i][j];
        				this.numero = 5;
        			}
        		};
        	break;
        	case 6:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece6[i][j];
        				this.numero = 6;
        			}
        		};
        	break;
        	case 7:   
        		for (int i=0;i<4;++i) {
        			for (int j=0;j<16;++j) { 
        				this.currentPiece [i][j]= piece7[i][j];
        				this.numero = 7;
        			}
        		};
        	break;
		}
	}
	
	
	public void currentPosition(int angle){

		this.currentPosition = new int [4][4];
		Random myRandom = new Random();
		
		// Pour parcourrir le tableau 4x4
		int k = 3; // ordonee
		int l = 0; // abscisse
		int i = angle; // on se place sur la rotation actuelle
		
		for(int j=0; j<16; ++j){
			this.currentPosition[l][k] = (this.currentPiece[i][j]*100);
			if(currentPosition[l][k]!=0)
				currentPosition[l][k] += myRandom.nextInt(26)+1; 
			l++;
			if(j == 3 || j == 7 || j == 11){
				k--;
				l = 0;
			}
		}
	}
	
	
	
	public void rotation(int angle){
		this.currentPosition(angle);
	}
	
	public void afficherCurrentPosition(){
		for(int i=3; i>=0; --i){ // ligne
			for(int j=0; j<4; ++j){ // colonne
				System.out.print( this.currentPosition[j][i] + " ");
			}
			System.out.print("\n");
		}
	}


	public int getType(){
		for(int i =0; i<4; ++i){
			for (int j=0; j<4; ++j){
				if(currentPosition[i][j]!=0){
					return currentPosition[i][j]/100;
				}
			}
		}
		return 0;
	}
	/*public static void main(String[] args) {
	Piece p = new Piece();
	p.creerPiece(1);
	p.currentPosition(0);
	p.afficherCurrentPosition();
	}*/

}
