package model;
import java.io.*;
import java.util.*;

public class Dico {
	
	HashSet <Mot>dictionnaire=new HashSet<Mot>();
	
	public Dico(String path) throws FileNotFoundException{
		Scanner scanner = new Scanner(new FileReader(path)); // ressources/dictionnaire
		 while (scanner.hasNextLine()) {
		     Mot mot = new Mot(scanner.nextLine());
		     dictionnaire.add(mot);
		 }
	}
	
	public boolean wordIsInDico(Mot myTry){
		return dictionnaire.contains(myTry);
	}


	/*public static void main(String[] args) throws FileNotFoundException {
		Dico myDico = new Dico("ressources/dictionnaire");

	}*/

}
