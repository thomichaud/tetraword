package model;
import java.awt.Cursor;

import javax.swing.ImageIcon;
import javax.swing.JButton;


public class Button extends JButton {

		public Button(int h, int w, int x, int y){
			this.setBounds(x, y, h, w);
			this.setFocusPainted(false);
			this.setBorderPainted(false);
			this.setContentAreaFilled(false);
			this.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
		
}
