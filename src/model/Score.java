package model;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class Score extends Component{
	
	public JPanel contentS;
	public JLabel scoreL;
	public int scoreValue;
	

	public void score(){
		contentS = new JPanel();
		contentS.setBackground(new Color(89,218,80));
		contentS.setBounds(395, 113, 100, 30);
		
		Font font = new Font("Arial",Font.BOLD,20);
		scoreL = new JLabel("0");
		scoreL.setFont(font);
		scoreL.setForeground(Color.white);
		
		scoreValue = 0;
		
		contentS.add(scoreL);
	}
	
	public void addGoodWord(Mot myMot){
		scoreValue += (myMot.myMot.length())*5;
		scoreL.setText("" + scoreValue);
	}
	
	public void addWrongWord(){
		scoreValue -= 5;
		scoreL.setText("" + scoreValue);
	}
	
	public void addGoodWordle(Mot myMot){
		System.out.println(myMot.length());
		scoreValue += (myMot.myMot.length())*7;
		scoreL.setText("" + scoreValue);
	}
}
