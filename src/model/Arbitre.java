package model;

import java.util.LinkedList;


/**
 	* Elle modelise l'arbitre du jeu
 	*/

public class Arbitre {


	Plateau myPlateau;
	int anagrammeDifficulty;
	// Score
	Dico myDico;
	
/**
     * Constructeur de la classe
     * @param myPlateauOther
     * 			Plateau sur lequel l'arbitre va agir
     * @param anagrammeDifficultyOther
     * 			DifficultŽ du mode anagramme
     * @param myDicoOther
     * 			Dictionnaire sur lequel l'arbitre va se reposer
     */	
	public Arbitre(Plateau myPlateauOther, int anagrammeDifficultyOther, Dico myDicoOther){
		myPlateau=myPlateauOther;
		anagrammeDifficulty=anagrammeDifficultyOther;
		myDico=myDicoOther;
	}

/**
     * Teste si un mot est correct
     * @param myWord
     * 			Mot ˆ controler
     * @return true si le mot est correct, false sinon
     */		
	public boolean wordIsOk (Mot myWord){
		if(!myDico.wordIsInDico(myWord))
			return false;
		else
			return true;
	}
	
/**
     * ImplŽmentation du mode anagrame
     * @param line
     * 			Ligne sur laquelle le mode est activŽe
     */
	public void modeAnagramme(int line, String mySelection, Score score){
		System.out.println("--- Mode anagramme ON---\n");
		Mot myMot = new Mot(mySelection);
		if(myMot.length()>=anagrammeDifficulty && myDico.wordIsInDico(myMot)){
			System.out.println("mot ok");
			score.addGoodWord(myMot);
			myPlateau.clearLine(line);
		}
		else{
			System.out.println("Mot KO -- MALUS");
			score.addWrongWord();
			myPlateau.clearLine(line);
		}
	}

/**
     * ImplŽmentation du mode wordle
     */		
	public void modeWordle(String mySelection, LinkedList<Point> myListCoord, Score score){
		System.out.println("--- Mode wordle ON---");
		Mot myMot = new Mot(mySelection);
		System.out.println(myMot.myMot);
		if(myDico.wordIsInDico(myMot)){
			System.out.println("Mot wordle OK\n");
			for(Point xety : myListCoord){
				myPlateau.deleteBrique(xety.x, xety.y);
			}
			myListCoord.clear();
			score.addGoodWordle(myMot);
		}
		else{
			System.out.println("Mot wordle KO\n");
			myListCoord.clear();
		}
	}
	
/**
     * Observe la situation du jeu en cours et agit en consŽquence : lance le mode anagramme si nŽcessaire ainsi que le mode wordle
     */	
	/*public void control(){
		boolean lineFull = true;
		boolean wordleMode = false;
		// on recherche si le mode anagramme doit �tre activŽ
		for(int i=1; i<25; ++i){
			lineFull=true;
			for(int j=0; j<10; ++j){
				if(myPlateau.myPlateau[j][i] == 0)
					lineFull=false;
					
			}
			if(lineFull){
				modeAnagramme(i);
				i--;
			}
		}
		
		
		if(wordleMode)
			modeWordle();
	}*/
	
}
