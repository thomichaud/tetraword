package model;


import java.util.*;
import java.applet.*;
import java.awt.*;
import java.awt.Graphics.*;


/**
        * Elle modelise le plateau de jeu
        */

public class Plateau extends Component{
	public int[][] myPlateau;
	public boolean isFull;
	
	int[] currentPosition = {3,21};
	Piece currentPiece = null;
	
/**
     * Constructeur
     */	
	public Plateau() {
		myPlateau = new int[10][25];
		isFull = false;
		
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				myPlateau[j][i] = 0;
			}
		}
		
		for(int i=0; i<10; ++i){
			myPlateau[i][0] = 800;
		}		
	}
/**
     * Affiche le plateau en mode console
     */		
	public void displayPlateau(){
		for(int i=24; i>=0; --i){
			for(int j=0; j<10; ++j){
				System.out.print(myPlateau[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n\n");
	}

/**
     * Setteur de la variable isFull
     */		
	
	public void setIsFull(boolean myBoolean){
		this.isFull=myBoolean;
	}

/**
     * Controle si le plateau deborde et met a jour la variable isFull
     */	
	public void isFull(){
		for(int i=24; i>20; --i){
			for(int j = 0; j<10; ++j){
				if(myPlateau[j][i]!=0){
					setIsFull(true);
					return;
				}
			}
		}
	}
	
/**
     * Efface entierement le plateau
     */		
	public void clearAll(){
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				myPlateau[j][i] = 0;
			}
		}
		
		for(int i=0; i<10; ++i){
			myPlateau[i][0] = 8;
		}
	}

/**
     * Efface une ligne du plateau
     * @param line
     * 			Numero de la ligne a supprimer
     */	
	public void clearLine(int line){
		for (int i=0; i<10; ++i){
			myPlateau[i][line] = 0;
		}
		for(int j=line; j<20; ++j){
			for(int i=0; i<10; ++i){
				myPlateau[i][j]=myPlateau[i][j+1];
			}
		for(int i=0; i<10; ++i){
				myPlateau[i][20]=0;
			}	
		}
		//Sound son = new Sound("clearLine.wav");
		//son.start();
	}

	
/**
     * Teste si une brique possede un element sur sa gauche
     * @param x
     * 			Abscisse de la brique de reference
     * @param y
     * 			Ordonnee de la brique de reference
     * @return true si la brique possede un element sur sa gauche
     */	
	public boolean haveElementInLeft(int x,int y){
		if(x>0 && myPlateau[x-1][y]!=0){
			return true;
		}
		return false;
	}

/**
     * Teste si une brique possede un element sur sa droite
     * @param x
     * 			Abscisse de la brique de reference
     * @param y
     * 			Ordonnee de la brique de reference
     * @return true si la brique possede un element sur sa droite
     */
	public boolean haveElementInRight(int x,int y){
		if(x<9 && myPlateau[x+1][y]!=0){
			return true;
		}
		return false;
	}

/**
     * Teste si une brique possede un element au dessus
     * @param x
     * 			Abscisse de la brique de reference
     * @param y
     * 			Ordonnee de la brique de reference
     * @return true si la brique possede un element au dessus
     */
	public boolean haveElementInTop(int x,int y){
		if(y<24 && myPlateau[x][y+1]!=0){
			return true;
		}
		return false;
	}
	
/**
     * Teste si une brique possede un element en dessous
     * @param x
     * 			Abscisse de la brique de reference
     * @param y
     * 			Ordonnee de la brique de reference
     * @return true si la brique possede un element en dessous
     */
	public boolean haveElementInBottom(int x,int y){
		if(y>1 && y<25 && myPlateau[x][y-1]!=0){ //x<25
			return true;
		}
		return false;
	}
	
/**
     * Place une piece en haut du plateau
     * @param myPiece
     * 			La piece a preparer
     */	
	public void setupCurrentPiece(Piece myPiece){
		currentPiece=myPiece;
		this.currentPosition[0]=3;
		this.currentPosition[1]=21;
	}
	
	
/**
     * Place une pi�ce au point (x,y)
     * 
     * @param x
     * 			Abscisse dans le plateau
     * @param y
     * 			Ordonn�e dans le plateau
     */	
	public void movePiece(int x, int y){
			for(int j=y; j<y+4; ++j){
				for(int i=x; i<x+4; ++i){
					if(j>0 && j<25 && i>=0 && i<10){
						if (myPlateau[i][j]==0){
							myPlateau[i][j]=currentPiece.currentPosition[i-x][j-y];
							
						}
					}
				}
			}
		
	}
	
/**
     * Retire une pi�ce au point (x,y)
     * 
     * @param x
     * 			Abscisse dans le plateau
     * @param y
     * 			Ordonn�e dans le plateau
     */	
	public void clearPiece(){
			for(int i=0; i<10; ++i){
				for(int j=1; j<25; ++j){
					if(isClearable(i, j))
						myPlateau[i][j]=0;
				}
			}
		}
	

/**
     * Renvoie l'ordonnée de la première brique non vide 
     * @param abs
     * 			Abscisse de recherche
     * @return -1 si aucune brique n'est dans la colonne d'abscisse abs
     */	
	public int searchBaseColumn(int abs){
		if(abs>-1 && abs <10){
			for(int i=0; i<4; ++i){
				if(i+currentPosition[1]>0){
					if(myPlateau[abs][i+currentPosition[1]]!=0 && (myPlateau[abs][i+currentPosition[1]]/100)==currentPiece.getType()){
					return i+currentPosition[1];
					}
				}
				
			}
		}
		return -1;
	}
	
/**
     * Renvoi l'abscisse de la brique non nulle la plus à gauche suivant la ligne passée en argument
     * @param ord
     * 			Ligne de recherche
     * @return -2 si aucune brique non vide n'est présente sur la ligne de recherche
     */			
	public int searchLeftLine(int ord){
		if(ord>0 && ord <21){
			for(int i=0; i<4; ++i){
				if(i+currentPosition[0]>=0 && i+currentPosition[0]<10){
					if(myPlateau[i+currentPosition[0]][ord]!=0 && (myPlateau[i+currentPosition[0]][ord]/100)==currentPiece.getType()){
					return i+currentPosition[0];
					}
				}
				
			}
		}
		return -2;
	}

/**
     * Renvoi l'abscisse de la brique non nulle la plus à droite suivant la ligne passée en argument
     * @param ord
     * 			Ligne de recherche
     * @return -2 si aucune brique non vide n'est présente sur la ligne de recherche
     */		
	public int searchRightLine(int ord){
		if(ord>0 && ord <21){
			for(int i=3; i>-1; --i){
				if(i+currentPosition[0]>=0 && i+currentPosition[0]<10){
					if(myPlateau[i+currentPosition[0]][ord]!=0 && (myPlateau[i+currentPosition[0]][ord]/100)==currentPiece.getType()){
					return i+currentPosition[0];
					}
				}
				
			}
		}
		return -2;
	}

/**
     * Teste si la pièce courante peut descendre
     * @return false si elle peut descendre 
     */		
	public boolean isDownable(){ // false = downable
		
		//on clone le tableau
		int [][] myTransposePlateau = new int[10][25];
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				if(myPlateau[j][i]==0)
					myTransposePlateau[j][i] = myPlateau[j][i];
				else
				 myTransposePlateau[j][i] = 900;
			}
		}
		
		for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
			for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
				if(j>=0 && j<10 && i>0 && i<25)
					myTransposePlateau[j][i] = currentPiece.currentPosition[j-currentPosition[0]][i-currentPosition[1]];
			}
		}
		
		for(int i=0; i<10; ++i){
			myTransposePlateau[i][0] = 800;
		}
		
		for(int i=currentPosition[0]; i<currentPosition[0]+4; ++i){ 
			if(searchBaseColumn(i)!=-1  && haveElementInBottom(i, searchBaseColumn(i)) && (myTransposePlateau[i][searchBaseColumn(i)]/100)==currentPiece.getType()){
				return true;
			}
			if(searchBaseColumn(i)==1){
				return true;
			}
		}
		
		return false;
		
	}

/**
     * Teste si la pièce courante peut se déplacer sur la gauche
     * @return false si elle peut se déplacer à gauche 
     */			
	public boolean isLeftable(){ // false = leftable
		
		//on clone le tableau
		int [][] myTransposePlateau = new int[10][25];
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				if(myPlateau[j][i]==0)
					myTransposePlateau[j][i] = myPlateau[j][i];
				else
				 myTransposePlateau[j][i] = 900; //why not ?
			}
		}
		
		for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
			for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
				if(j>=0 && j<10 && i>0 && i<25)
					myTransposePlateau[j][i] = currentPiece.currentPosition[j-currentPosition[0]][i-currentPosition[1]];
			}
		}
		
		for(int i=0; i<10; ++i){
			myTransposePlateau[i][0] = 800;
		}
		
		for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){ 
			if(searchLeftLine(i)!=-2  && haveElementInLeft(searchLeftLine(i), i) && (myTransposePlateau[searchLeftLine(i)][i]/100)==currentPiece.getType()){
				return true;
			}
			if(searchLeftLine(i)==0){
				return true;
			}
		}
		
		return false;
		
	}

/**
     * Teste si la pièce courante peut se déplacer sur la droite
     * @return false si elle peut se déplacer à droite 
     */		
public boolean isRightable(){ // false = leftable
		
		//on clone le tableau
		int [][] myTransposePlateau = new int[10][25];
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				if(myPlateau[j][i]==0)
					myTransposePlateau[j][i] = myPlateau[j][i];
				else
				 myTransposePlateau[j][i] = 900; //why not ?
			}
		}
		
		for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
			for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
				if(j>=0 && j<10 && i>0 && i<25)
					myTransposePlateau[j][i] = currentPiece.currentPosition[j-currentPosition[0]][i-currentPosition[1]];
			}
		}
		
		for(int i=0; i<10; ++i){
			myTransposePlateau[i][0] = 800;
		}
		
		for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){ 
			if(searchRightLine(i)!=-2  && haveElementInRight(searchRightLine(i), i) && (myTransposePlateau[searchRightLine(i)][i]/100)==currentPiece.getType()){
				return true;
			}
			if(searchRightLine(i)==9){
				return true;
			}
		}
		
		return false;
		
	}


/**
 	* Teste si une zone du plateau  peut être effacée
 	* @return true si la brique de coordonnée (x,y) peut être effacée 
 	*/	
public boolean isClearable(int x, int y){
	int [][] myTransposePlateau = new int[10][25];
	for(int i=1; i<25; ++i){
		for(int j=0; j<10; ++j){
			if(myPlateau[j][i]==0)
				myTransposePlateau[j][i] = myPlateau[j][i];
			else
			 myTransposePlateau[j][i] = 900; //why not ?
		}
	}
	
	for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
		for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
			if(j>=0 && j<10 && i>0 && i<25)
				myTransposePlateau[j][i] = currentPiece.currentPosition[j-currentPosition[0]][i-currentPosition[1]];
		}
	}
	
	for(int i=0; i<10; ++i){
		myTransposePlateau[i][0] = 800;
	}
	if((myTransposePlateau[x][y]/100)==currentPiece.getType())
		return true;
	else
		return false;

}

/**
 	* Teste si la pièce courante peut subir une rotation
 	* @return true si elle peut subir une rotation
 	*/	
public boolean isRotatable(){
	
	int [][] myTransposePlateau = new int[10][25];
	for(int i=1; i<25; ++i){
		for(int j=0; j<10; ++j){
			if(myPlateau[j][i]==0)
				myTransposePlateau[j][i] = myPlateau[j][i];
			else
			 myTransposePlateau[j][i] = 900; //why not ?
		}
	}
	
	for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
		for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
			if(j>=0 && j<10 && i>0 && i<25)
				myTransposePlateau[j][i] = currentPiece.currentPosition[j-currentPosition[0]][i-currentPosition[1]];
		}
	}
	
	for(int i=0; i<10; ++i){
		myTransposePlateau[i][0] = 800;
	}
	
	for(int i=currentPosition[1]; i<currentPosition[1]+4; ++i){
		for(int j=currentPosition[0]; j<currentPosition[0]+4; ++j){
			if(j>=0 && j<10 && i>0 && i<25){
				if(myTransposePlateau[j][i]==900) {
					return false;
			}
		}
	}
	}
	
	
	if(currentPosition[1]>=1 && currentPosition[1]<=21){
		if(currentPosition[0]>=0 && currentPosition[0]<6){
			return true;
		}
		else
			return false;
	}
	else
		return false;

}

/**
     * Descend la piece courante d'une unite vers le bas
     * @return true si, une fois descendue, la brique ne doit plus descendre
     */	
	public boolean downCurrentPiece(){
		
		if(!isDownable()){	
			clearPiece();
			this.currentPosition[1]--;
			movePiece(currentPosition[0], currentPosition[1]);
			return false;
		}
		else
			return true;
	}
	
/**
     * Bouge la piece courante d'une unite vers la gauche
     */	
	public void leftCurrentPiece(){
		if(!isLeftable()){	
			clearPiece();
			this.currentPosition[0]--;
			movePiece(currentPosition[0], currentPosition[1]);
		}
	}
	
/**
     * Bouge la piece courante d'une unite vers la droite
     */	
	public void rightCurrentPiece(){
		if(!isRightable()){	
			clearPiece();
			this.currentPosition[0]++;
			movePiece(currentPosition[0], currentPosition[1]);
		}
	}

/**
     * Descend la piece courante au maximum sur la plateau
     * @return true si, une fois descendue, la brique ne doit plus descendre
     */		
	public boolean downSpaceCurrentPiece(){
		while(!isDownable()){
			downCurrentPiece();
			if(currentPosition[1]==1){
				return true;
			}
		}
		return true;
	}

/**
     * Effectue la rotation de la pièce courante
     */	
	public void rotateCurrentPiece(){
		clearPiece();
		if(isRotatable()){
			System.out.println("Rotation \n");	
			Sound son = new Sound("rotation.wav");
			son.start();
			currentPiece.countRotation++;
			if (currentPiece.countRotation == 4){
				currentPiece.countRotation = 0;
			}
			currentPiece.currentPosition(currentPiece.countRotation);
			
		}
		
	}
	
/**
     * Retourne le type de brique en (x,y)
     * @param x
     * 			Abscisse de la brique
     * @param y
     * 			Ordonnée de la brique
     */	
	public int getBriqueType(int x, int y){
		return myPlateau[x][y]/100;
		
	}
	
/**
     * Retourne la lettre de la brique en (x,y)
     * @param x
     * 			Abscisse de la brique
     * @param y
     * 			Ordonnée de la brique
     */	
	public char getBriqueLetter(int x, int y){
		return (char) ((char)myPlateau[x][y]-(getBriqueType(x, y)*100)+64);
		
	}
	
/**
     * Efface la brique en (x,y)
     * @param x
     * 			Abscisse de la brique
     * @param y
     * 			Ordonnée de la brique
     */	
	public void deleteBrique(int x, int y){
		if(myPlateau[x][y]!=0){
			myPlateau[x][y]=0;
			for(int j=y; j<21; ++j){
					myPlateau[x][j]=myPlateau[x][j+1];
			}
		}
	}

	
/**
     * Teste si le plateau a au moins une ligne pleine
     */	
	public boolean isLineFull(){
		for(int i=1; i<25; ++i){
			for(int j=0; j<10; ++j){
				if(myPlateau[j][i] == 0)
					return false;
			}
		}
		return true;
	}
	
	
	/*public static void main(String[] args) {
	
		Plateau myPlateau = new Plateau();
		
		Piece p;
		p = new Piece();
		
		p.creerPiece(1);
		p.currentPosition(0);
		
		myPlateau.setupCurrentPiece(p);
		
		myPlateau.downCurrentPiece();
		myPlateau.downCurrentPiece();
		myPlateau.downCurrentPiece();
		myPlateau.downCurrentPiece();
		
		myPlateau.displayPlateau();
		
		for(int i=1; i<=5; ++i){
			myPlateau.rightCurrentPiece();
		}
		
		myPlateau.displayPlateau();
	}*/
}

	
