package model;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;
 
public class Interface extends JPanel {

	public void paintComponent(Graphics g){
	    try {
	    	Image background = ImageIO.read(new File("img/background.png"));
	    	Image score = ImageIO.read(new File("img/score.png"));
	    	Image annagramme = ImageIO.read(new File("img/annagrame.png"));
	    	Image plateau = ImageIO.read(new File("img/plateau.png"));
	    	Image nextPiece = ImageIO.read(new File("img/nextPiece.png"));
	    	Image bonus = ImageIO.read(new File("img/bonus.png"));
	    	Image logo = ImageIO.read(new File("img/logo.png"));
	    	
		    g.drawImage(background, 0, 0, this.getWidth(), this.getHeight(), this);
		    g.drawImage(score,320, 100, this);
		    g.drawImage(annagramme,320, 340, this);
		    g.drawImage(plateau,30, 90, this);
		    g.drawImage(nextPiece,330, 180, this);
		    g.drawImage(bonus,450, 200, this);
		    g.drawImage(logo,430, 10, this);
		    
		    }
	    catch (IOException e) {
		      e.printStackTrace();
		}   
	}
}
