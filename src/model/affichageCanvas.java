package model;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

public class affichageCanvas extends Canvas implements Runnable {
	 
	public void start(){
		Thread t = new Thread(this);
		t.setPriority(Thread.MAX_PRIORITY);
		t.start();
	}
	
	public void dessinePlateau(Plateau pl) {
		boolean running = true;
		while (running){
			BufferStrategy buf = getBufferStrategy();
			if(buf == null){
				createBufferStrategy(3);
				continue;
			}
			Graphics2D g = (Graphics2D) buf.getDrawGraphics();
			renderPlateau(g, pl);
			buf.show();
			running = false;
		}
	}
	
	public void dessinePiece(Piece p) {
		boolean running = true;
		while (running){
			BufferStrategy buf = getBufferStrategy();
			if(buf == null){
				createBufferStrategy(3);
				continue;
			}
			Graphics2D g = (Graphics2D) buf.getDrawGraphics();
			renderNextPiece(g, p);
			buf.show();
			running = false;
		}
	}
	
	void renderPlateau(Graphics2D g, Plateau pl){
		g.setColor(new Color(174, 152, 115));
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for (int j=21;j>=0;--j) {
			for (int i=0;i<10;++i) {
				if (pl.getBriqueType(i, j) == 0){
					g.setColor(new Color(139, 116, 76));
					g.fillRect(20*i,20*(20-j),20,20);
				}
				else{
					switch(pl.getBriqueType(i, j)){
					
					case 1 :
						g.setColor(new Color(246, 233, 26));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar1 = pl.getBriqueLetter(i, j);
						String letter1 = Character.toString(letterChar1); 
						g.drawString(letter1, 6+20*i,15+20*(20-j));
					break;
					case 2 :
						g.setColor(new Color(41, 229, 227));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar2 = pl.getBriqueLetter(i, j);
						String letter2 = Character.toString(letterChar2); 
						g.drawString(letter2, 6+20*i,15+20*(20-j));
					break;
					case 3 :
						g.setColor(new Color(32, 238, 66));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar3 = pl.getBriqueLetter(i, j);
						String letter3 = Character.toString(letterChar3); 
						g.drawString(letter3, 6+20*i,15+20*(20-j));
					break;
					case 4 :
						g.setColor(new Color(238, 32, 32));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar4 = pl.getBriqueLetter(i, j);
						String letter4 = Character.toString(letterChar4); 
						g.drawString(letter4, 6+20*i,15+20*(20-j));
					break;
					case 5 :
						g.setColor(new Color(114, 69, 124));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar5 = pl.getBriqueLetter(i, j);
						String letter5 = Character.toString(letterChar5); 
						g.drawString(letter5, 6+20*i,15+20*(20-j));
					break;
					case 6 :
						g.setColor(new Color(31, 103, 191));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar6 = pl.getBriqueLetter(i, j);
						String letter6 = Character.toString(letterChar6); 
						g.drawString(letter6, 6+20*i,15+20*(20-j));
					break;
					case 7 :
						g.setColor(new Color(246, 180, 21));
						g.fillRect(20*i,20*(20-j),20,20);
						g.setColor(Color.white);
						char letterChar7 = pl.getBriqueLetter(i, j);
						String letter7 = Character.toString(letterChar7); 
						g.drawString(letter7, 6+20*i, 15+20*(20-j));
					break;
					case 8:
						g.setColor(new Color(227, 191, 133));
						g.fillRect(20*i,20*(20-j),20,20);
					break;
					}
				}
			}
		}
	}
	
	void renderNextPiece(Graphics2D g, Piece p){
		g.setColor(new Color(174, 152, 115));
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for (int i=0;i<4;++i) {
			for (int j=0;j<4;++j) {
				if (p.currentPosition[i][j] == 0){
					g.setColor( new Color(227, 191, 133));
					g.fillRect(15*i,15*(3-j),15,15);
				}
				else{
					if(p.numero == 1){
						g.setColor(new Color(246, 233, 26));
						g.fillRect(15*i,15*(3-j),15,15);
					}
					else if(p.numero == 2){
						g.setColor(new Color(41, 229, 227));
						g.fillRect(15*i,15*(3-j),15,15);
					}
					else if(p.numero == 3){
						g.setColor(new Color(32, 238, 66));
						g.fillRect(15*i,15*(3-j),15,15);
					}
					else if(p.numero == 4){
						g.setColor(new Color(238, 32, 32));
						g.fillRect(15*i,15*(3-j),15,15);
					}

					else if(p.numero == 45){
						g.setColor(new Color(114, 69, 124));
						g.fillRect(15*i,15*(3-j),15,15);
					}

					else if(p.numero == 6){
						g.setColor(new Color(31, 103, 191));
						g.fillRect(15*i,15*(3-j),15,15);
					}

					else if(p.numero == 7){
						g.setColor(new Color(246, 180, 21));
						g.fillRect(15*i,15*(3-j),15,15);
					}
				}
			}
		}		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
	
	}