package model;

import java.util.Timer;
import java.util.TimerTask;


public class Update {
	    Timer timer;
	    Plateau myPlateau;
	    public static boolean pieceCurrentUp = false;
	    boolean pauseBoolUp;
	    affichageCanvas tet2Up;
	    affichageCanvas nextPieceUp;
	    Piece npUp;

	    public Update(int mseconds, Plateau myPlateauOther, boolean pauseBoolOther, affichageCanvas tet2Other, affichageCanvas nextPieceOther, Piece npOther) {
	        timer = new Timer();  //At this line a new Thread will be created
	        timer.schedule(new RemindTask(myPlateau), mseconds); //delay in milliseconds
	        this.myPlateau = myPlateauOther;
	        this.pauseBoolUp = pauseBoolOther;
	        this.tet2Up = tet2Other;
	        this.nextPieceUp = nextPieceOther;
	        this.npUp = npOther;
	        
	        while(!pieceCurrentUp){
				if(pauseBoolUp == false){
					//pieceCurrent=myPlateau.downCurrentPiece();
					
					// plateau jeu
					tet2Up.setBounds(50, 115, 199, 407);
					tet2Up.dessinePlateau(myPlateau);
					// nextPiece
					
					nextPieceUp.setBounds(360, 200, 60, 60);
					nextPieceUp.dessinePiece(npUp);
				}
	        }
	    }

	    class RemindTask extends TimerTask{

	    	Plateau myPlateau;

	    	
	    	public RemindTask(Plateau plateauOther){
	    		this.myPlateau = plateauOther;

	    	}
	        @Override
	        public void run() {
						Update.pieceCurrentUp=myPlateau.downCurrentPiece();

	        
	    }


}
	    }
	       
	    
