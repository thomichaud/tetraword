package model;


public class Mot {
	public String myMot;
	
	public Mot(String myString){
		this.myMot=myString.toLowerCase();
	}
	
	public int length(){
		return myMot.length();
	}
	
	public boolean equals(Object o){
		if (!(o instanceof Mot))
			return false;
		Mot motTest = (Mot) o;
		if (!(this.myMot.equals(motTest.myMot)))
			return false;
		return true;
	}
	
	
	public int hashCode(){
		int h = 0;
		int c = 42;
		// �tape 1 - Hachage
		for(int i = 0 ; i < this.myMot.length() ; i++) // On parcourt tous les caract�res
			h += (int) this.myMot.charAt(i) * (int)(Math.pow(c, i));
		return h;
	}
	


}
