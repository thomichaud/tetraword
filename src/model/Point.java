package model;

public class Point {
	public int x = 0;
	public int y = 0;
	
	public Point (int myX, int myY){
		this.x=myX;
		this.y=myY;
	}
}
