package model;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import javax.swing.JFrame;

public class Main extends Canvas implements Runnable {

	public static final int WIDTH = 150, HEIGHT = 390;
	
	public static void main(String[] args) {
			
			/* Piece */
			Piece p;
			p = new Piece();
			p.creerPiece(2);
			p.currentPosition(0);
			//p.afficherCurrentPosition();
			System.out.println();
			
			/* Plateau */
			Plateau myPlateau = new Plateau();
			
			myPlateau.setupCurrentPiece(p);
			myPlateau.displayPlateau();
			
			myPlateau.downCurrentPiece();
			myPlateau.displayPlateau();
			
			myPlateau.downSpaceCurrentPiece();
			myPlateau.displayPlateau();
			
			/* Interface */
			
			 Main tet = new Main();
			
			JFrame frame = new JFrame();
			frame.setSize(WIDTH, HEIGHT);
			System.out.println(frame.getWidth());
			System.out.println(frame.getHeight());
			frame.setLocationRelativeTo(null);
			//frame.setContentPane(tet);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);
		   
		   
		    frame.add(tet);
		    frame.setVisible(true);
			
		    tet.start();
		    tet.dessine(myPlateau);

	}
	
	public void start(){
		Thread t = new Thread(this);
		t.setPriority(Thread.MAX_PRIORITY);
		t.start();
	}
	
	public void dessine(Plateau pl) {
		boolean running = true;
		while (running){
			BufferStrategy buf = getBufferStrategy();
			if(buf == null){
				createBufferStrategy(3);
				continue;
			}
			Graphics2D g = (Graphics2D) buf.getDrawGraphics();
			render(g, pl);
			buf.show();
		}
	}
	
	public void render(Graphics2D g, Plateau pl){
		g.setColor(Color.gray);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		for (int j=24;j>=0;--j) {
			for (int i=0;i<10;++i) {
				if (pl.myPlateau[i][j] == 0){
					g.setColor(Color.yellow);
					g.fillRect(15*i,15*(23-j),15,15);;
				}
				else{
					switch(pl.myPlateau[i][j]){
					
					case 1 :
						g.setColor(Color.red);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 2 :
						g.setColor(Color.blue);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 3 :
						g.setColor(Color.green);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 4 :
						g.setColor(Color.pink);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 5 :
						g.setColor(Color.orange);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 6 :
						g.setColor(Color.gray);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 7 :
						g.setColor(Color.black);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					case 8:
						g.setColor(Color.white);
						g.fillRect(15*i,15*(23-j),15,15);
					break;
					}
				}
			}
				System.out.println();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}	
}
