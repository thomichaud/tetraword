package main;

import model.Arbitre;
import model.Dico;
import model.Mot;
import model.Piece;
import model.Interface;
import model.Button;
import model.Plateau;
import model.Score;
import model.Sound;
import model.affichageCanvas;
import model.Point;

import java.util.LinkedList;
import java.util.Random;
import java.applet.Applet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.lang.Math;




public class Main {

	public static final int WIDTH = 548, HEIGHT = 600;
	

	public static affichageCanvas tet2 = new affichageCanvas();
	
	public static Plateau myPlateau;
	public static Random myRandom;
	public static Piece p;
	public static Piece np;

	public static JFrame frame;

	public static boolean pieceCurrent = false;
	public static boolean pauseBool = false;
	public static boolean activeWordle = false;
	public static boolean pauseWordle = false;
	
	public 
	
	static int countRotation = 0;
	public static boolean lineIsFull = true;
	
	public static String mot ="";
	public static int lineToReturn = 0;
	
	public static JLabel word;
	public static JPanel contentW;

	public static Dico myDico;
	public static Arbitre myArbitre;
	
	public static Score score;
	
	public static LinkedList<Point> WordlePosition = new LinkedList<Point>();

	public static void main(String[] args) throws IOException {

		contentW = new JPanel();
		contentW.setBackground(new Color(233,189,37));
		contentW.setBounds(375, 355, 120, 30);

		Font font = new Font("Arial",Font.BOLD,20);
		word = new JLabel("- - - - - -");
		word.setFont(font);
		word.setForeground(Color.white);
		word.setFont( word.getFont().deriveFont(Font.BOLD));
		
		score = new Score();
		score.score();
				
		myPlateau = new Plateau();
		myRandom  = new Random();
		
		myDico = new Dico("ressources/dictionnaire");
		myArbitre = new Arbitre(myPlateau, 2, myDico);
		
		p = new Piece();
		p.creerPiece(myRandom.nextInt(7)+1);
		p.currentPosition(0);
		
		np = new Piece();
		np.creerPiece(myRandom.nextInt(7)+1);
		np.currentPosition(0);
	
		/*CONTENEUR PLATEAU*/
		
		
		/*AFFICHAGE NEXT PIECE*/
		affichageCanvas nextPiece = new affichageCanvas();
		
		frame = new JFrame();
		
		/*INTERFACE*/
		Interface I = new Interface();
		
		/*EVENT BUTTON*/
		Button  option = new Button(60, 60, 470, 500);
		option.setIcon(new ImageIcon("img/optionB.png"));
		Button  pause = new Button(60, 60, 405, 500);
		pause.setIcon(new ImageIcon("img/pauseB.png"));
				
	    pause.addActionListener(new pauseButtonListener());
	    option.addActionListener(new optionButtonListener());
		
	    /*EVENT CLAVIER*/
	    p.setFocusable(true);
	    p.requestFocus();
	    p.addKeyListener(new pListener());
	    
	    /*EVENT SOURIS*/
	    tet2.addMouseListener(new plateauListener());
	    
		frame.setSize(WIDTH, HEIGHT);

		/*Composants de la fenetres*/
		contentW.add(word);
		frame.add(contentW);
		frame.add(score.contentS);
		frame.add(tet2);
		frame.add(nextPiece);
	    frame.add(option);
	    frame.add(pause);
	    frame.add(p);
	    frame.add(I);

  
	    /* actions sur les boutons*/
	    
	    /*paramêtre de la fenetre */
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);

	    frame.setVisible(true);

	   
	    /*lancement des threads*/
		tet2.start();
		nextPiece.start();

		while(!myPlateau.isFull){
			pieceCurrent=false;
			myPlateau.setupCurrentPiece(p);
			
			while(!pieceCurrent){
				if(pauseBool == false){
					pieceCurrent=myPlateau.downCurrentPiece();
					
					// plateau jeu
					tet2.setBounds(50, 115, 199, 407);
					tet2.dessinePlateau(myPlateau);
					// nextPiece
					
					nextPiece.setBounds(360, 200, 60, 60);
					nextPiece.dessinePiece(np);
					try{
						Thread.sleep(500);
						}catch(InterruptedException e){}
				}
			}
			if(pauseWordle == true)
				pauseBool = true;
			else
				pauseBool = false;
	
			for(int i=1; i<25; ++i){
				lineIsFull=true;
				for(int j=0; j<10; ++j){
					if(myPlateau.myPlateau[j][i] == 0)
						lineIsFull=false;
				}
				if(lineIsFull){
					pauseBool = true;
					System.out.println("Le jeu est sur pause");
					lineToReturn = i;
					System.out.println("ligne complete : " + lineToReturn);
					//i = 25;	
					//tet2.requestFocus();
					//myArbitre.modeAnagramme(i);
					//i--;
				}
			}
			
			p.creerPiece(np.numero);
			p.currentPosition(0);
			np.creerPiece(myRandom.nextInt(7)+1);
			np.currentPosition(0);
			
			myPlateau.isFull();
		}
	}
	
	static class plateauListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent arg0) {
		    
			int y = 20-(arg0.getY()/20);
			int x = arg0.getX()/20;
			
			Point myCurrentPoint = new Point(x, y);

			if(lineToReturn != 0){
				if(y == lineToReturn){
					mot+= myPlateau.getBriqueLetter(x, y);
					word.setText(mot);
					System.out.println(mot);
				}
				else{
					System.out.println("vous ne cliquez pas sur la bonne ligne");
				}
			}
			else {
				System.out.println("pas en mode Annagrame");
				
				if(activeWordle == true){
					if(WordlePosition.size() == 0){
						System.out.println("liste vide");
						WordlePosition.add(myCurrentPoint);
						mot+= myPlateau.getBriqueLetter(x, y);
					}
					else{
						System.out.println("liste un peu remplie");
						int valAbsX= Math.abs(WordlePosition.getLast().x - myCurrentPoint.x);
						int valAbsY = Math.abs(WordlePosition.getLast().y - myCurrentPoint.y);
						System.out.println("absX : " + valAbsX + " absY : " + valAbsY);
						if(valAbsX <= 1 &&  valAbsY <= 1){
							if(valAbsX == 0 && valAbsY == 0){
								System.out.println("je clique sur la m�me case");
							}
							else{
								//System.out.println("coord pt : " + myCurrentPoint.x + ", " + myCurrentPoint.y);
								//System.out.println("coord pt precedent : " + WordlePosition.getLast().x + ", " + WordlePosition.getLast().y);
								 System.out.println("coucou je suis ton voisin");
								WordlePosition.add(myCurrentPoint);
								mot+= myPlateau.getBriqueLetter(x, y);
								pauseWordle = true;
							}
						}
						else{
							System.out.println("Selection non valide");
						}
					}
				}
				else
					System.out.println("pas en mode wordle");
			}
			p.requestFocus();
			
			
		}

		private int abs(int i) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public void mouseEntered(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent arg0) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	static class pauseButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if (pauseBool == false){
				pauseBool = true;
				System.out.println("Le jeu est sur pause");
			}
			else{
				pauseBool = false;
				System.out.println("Le jeu reprend");
			}

		    p.requestFocus();
		}
	}
	
	static class optionButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			/*optionPress = true;
			if(optionPress == true){
				frame.add(quitO);
			}*/
		}
	}
	
	static class pListener implements KeyListener{

		@Override
		public void keyPressed(KeyEvent e) {
			switch (e.getKeyCode())
			{
				case KeyEvent.VK_RIGHT:
					myPlateau.rightCurrentPiece();
					break;
				case KeyEvent.VK_LEFT:
					myPlateau.leftCurrentPiece();
					break;
				case KeyEvent.VK_UP:
					myPlateau.rotateCurrentPiece();
					System.out.println("haut");
					break;
				case KeyEvent.VK_DOWN:
					myPlateau.downCurrentPiece();
					break;
				case KeyEvent.VK_W:
					activeWordle = true;
					pauseWordle = true;
				break;
				case KeyEvent.VK_SPACE:
					myPlateau.downSpaceCurrentPiece();
				break;
				case KeyEvent.VK_ENTER:
					if(activeWordle == true){
						myArbitre.modeWordle(mot, WordlePosition, score);
						mot = "";
						pauseBool = false;
						activeWordle=false;
						pauseWordle = false;
					}
					else{
						myArbitre.modeAnagramme(lineToReturn, mot, score);
						mot = "";
						word.setText("- - - - - -");
						pauseBool = false;
					}
					break;
			}
			
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub
			
		}
	
	}
}
